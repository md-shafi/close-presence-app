package se.hel.closepresence.service.networking.objects;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

/**
 * Created by k on 2016-08-18.
 */
public class OutOfRangeBody {

    //input={"api_key":"28742sk238sdkAdhfue243jdfhvnsa1923347","id_user":"4","beacon_uuid":"ABCD1234","timestamp":"323232"}
    String api_key, beacon_uuid;
    int id_user;
    int timestamp = 0;

    public OutOfRangeBody(String api_key, Integer userid, String beaconUUID) {
        this.api_key=api_key;
        this.beacon_uuid=beaconUUID;
        this.id_user=userid;
    }
    //{"api_key":"28742sk238sdkAdhfue243jdfhvnsa1923347","id_user":"4","beacon_uuid":"ABCD1234","timestamp":"323232"}



}
