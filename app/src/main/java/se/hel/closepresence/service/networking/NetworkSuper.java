package se.hel.closepresence.service.networking;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Superclass for all network calls.
 * Provides class variables and methods for checking if a network call has been completed, and completed successfully.
 * Additionally provides ProgressDialogs.
 * NOTE: isOk may be null if the request was not yet completed. Add checks, or at least check if isDone to avoid nullpointerexceptions
 */
public abstract class NetworkSuper<T> {
    protected boolean ok = false;
    protected boolean done = false;

    protected Context ourcontext; //Store the context so that we may dismiss the ProgressDialog when done

    public ProgressDialog getPd() {
        return pd;
    }

    protected ProgressDialog pd;
    protected T result; //Generic result
    public boolean isDone()
    {
        return done;
    }

    public boolean isOk()
    {
        return ok;
    }

    protected void setOk(boolean ok) {
        this.ok = ok;
    }

    protected void setDone(boolean done) {
        this.done = done;
    }

    protected void setResult(T result)
    {
        this.result = result;
    }

    public abstract T getResult();



}
