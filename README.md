# Close Presence

An Android app that receives messages from a Bluetooth Low Energy (BLE) device when a user comes within range and reports the event to a server.